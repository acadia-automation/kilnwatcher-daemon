# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
from typing import List, Any

from db.influx_stats_transmitter import InfluxStatsTransmitter
from utils.config import Configuration
from utils.kilnwatcher_errors import PeripheralResponseError
from watchers.watcher import Watcher

logger = logging.getLogger(__name__)


class KilnTempWatcher(Watcher):

    def __init__(self, kiln_temp_sensor, config: Configuration, watcher_name: str, watcher_log_name: str):
        super().__init__(config, watcher_name, watcher_log_name)
        self.__kiln_temp_sensor = kiln_temp_sensor
        # This isn't thread safe, but there should only be one of these threads
        self.__current_kiln_temp = -1

    def _get_measurement_interval_config_name(self):
        return Configuration.TEMPERATURE_MEASUREMENT_INTERVAL

    def _gather_watch_resources(self) -> list:
        return []

    def _take_one_look(self, stats_transmitter: InfluxStatsTransmitter, resources: List[Any]):
        try:
            self.__current_kiln_temp = self.__kiln_temp_sensor.read_temperature()
            self.__transmit_metrics(self.__current_kiln_temp, stats_transmitter)
        except PeripheralResponseError as pre:
            logger.error("Failed to read from kiln temp sensor.")
            logger.exception(pre)

    def have_you_seen_enough(self) -> bool:
        return self.__current_kiln_temp < self._config.get_int(Configuration.KILN_TEMP_WARNING_THRESHOLD)

    # Utility methods
    @staticmethod
    def __transmit_metrics(temperature, stats_transmitter):
        stats_transmitter.transmit(
            "device_temperature_sensor",
            {"device": "kiln", "location": "internal", "sensor_type": "KTypeThermocouple"},
            {"temperature": temperature})
