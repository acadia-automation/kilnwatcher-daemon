# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import time

from peripherals.range_indicator import RangeIndicator
from peripherals.seven_segment_display import SevenSegmentDisplay
from peripherals.temp_sensor import TempSensor
from utils.config import Configuration
from watchers.environmental_daemon import EnvironmentalDaemon

logger = logging.getLogger(__name__)


class KilnTempDaemon(EnvironmentalDaemon):

    def __init__(self, config: Configuration, kiln_temp_sensor: TempSensor, kiln_temp_indicator: RangeIndicator,
                 kiln_temp_display: SevenSegmentDisplay, daemon_name: str, daemon_log_name: str):
        """
        param: config
        Configuration
            The configuration repository that this daemon should use for various parameters
        param: kiln_temp_sensor
        TempSensor
            The case sensor that should be monitored by this watcher daemon.
        param: kiln_temp_indicator
        Indicator
            The indicator that should be enabled when there is a high temperature warning
        param: kiln_temp_display
        SevenSegmentDisplay
            The numeric display that should show the kiln temperature when the temperature is over the warning threshold
        """
        super().__init__(config.get_int(Configuration.TEMPERATURE_MEASUREMENT_INTERVAL), daemon_name, daemon_log_name)
        self.__kiln_temp_sensor = kiln_temp_sensor
        self.__kiln_temp_indicator = kiln_temp_indicator
        self.__kiln_temp_display = kiln_temp_display

        self.__temp_threshold = config.get_int(Configuration.KILN_TEMP_WARNING_THRESHOLD)

    def _stop_peripherals(self):
        self.__kiln_temp_indicator.off()
        self.__kiln_temp_display.off()

    def _single_possession(self):
        # Read the kiln case temperature
        kiln_temp = self.__kiln_temp_sensor.read_temperature()

        # Check to see if temperature is above threshold
        if kiln_temp > self.__temp_threshold:
            # Kiln is hot, light indicator
            self.__kiln_temp_indicator.range_on(kiln_temp)
            # Display current case temperature
            self.__kiln_temp_display.display(kiln_temp)
        else:
            # Kiln is not hot, indicator off
            self.__kiln_temp_indicator.off()
            # Display the time, as a convenience
            self.__kiln_temp_display.display(time.strftime("%I:%M"))

    def _broadcast_error(self):
        self.__kiln_temp_indicator.warning()
        self.__kiln_temp_display.error()
