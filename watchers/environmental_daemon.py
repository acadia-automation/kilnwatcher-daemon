# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import threading
import time

from utils.kilnwatcher_errors import PeripheralResponseError

logger = logging.getLogger(__name__)


class EnvironmentalDaemon:
    """
    A class used to monitor the temperature of the kiln case exterior and the
    ambient temperature. This class spawns, controls, and disbands a thread to
    do this work. The thread is similar to a Daemon thread , in that it is
    intended to be long lived and run constantly while the controlling process
    runs, however, it should be shut down cleanly, rather than letting it die
    with the process, as there are displays and indicators that could be left
    in a misleading state if this thread just dies.
    """

    def __init__(self, sleep_interval: int, daemon_name: str, daemon_log_name: str):
        """
        Abstract class for persistent environmental monitoring daemons. Each
        daemon should be responsible for a single environmental factor.

        :param daemon_name
        str
            friendly name this daemon should be called
        :param daemon_log_name
        str
            short name used to refer to daemon thread frequently in logs
        """
        self.__summoned = False
        self.__daemon_thread = None
        self._daemon_name = daemon_name
        self._daemon_log_name = daemon_log_name
        self._sleep_interval = sleep_interval

    def i_summon_you(self):
        """
        Initiates the primary activities of this class. This method executes and
        returns, but spawns a thread that will execute the monitoring activities
        that this class is responsible for.
        """
        if self.__summoned and self.__daemon_thread:
            logger.warning(f"{self._daemon_name} was summoned, but there is already a daemon possessing this vessel.")
            return

        logger.debug("Summoning environmental Daemon.")
        self.__summoned = True
        self.__daemon_thread = threading.Thread(target=self.__possess, name=self._daemon_log_name, args=(1,))
        self.__daemon_thread.start()
        logger.debug(f"Daemon thread: {self.__daemon_thread.name}")

    def i_banish_you(self):
        """
        Cleanly stops the activities of this class. It will wait for the thread
        to complete and stop prior to returning.
        """
        if not self.__summoned:
            logger.warning(f"{self._daemon_name} was banished, but this vessel is not possessed.")
            return

        self.__summoned = False
        self.__daemon_thread.join()
        try:
            self._stop_peripherals()
        except PeripheralResponseError:
            logger.error(f"Problem turning off peripherals during {self._daemon_name} shutdown.")

        logger.info("I now take my leave.")

    def __possess(self, name):

        try:
            logger.info(f"{self._daemon_name} is now possessing the kiln.")
            while self.__summoned:
                try:
                    self._single_possession()
                except PeripheralResponseError as pre:
                    logger.error(f"Peripheral Response exception encountered")
                    logger.exception(pre)
                    self._broadcast_error()

                time.sleep(self._sleep_interval)
        except Exception as e:
            logger.error(f"Unexpected exception encountered")
            logger.exception(e)

    def _stop_peripherals(self):
        raise NotImplementedError()

    def _single_possession(self):
        raise NotImplementedError()

    def _broadcast_error(self):
        raise NotImplementedError()
