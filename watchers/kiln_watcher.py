# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import signal
import sys
import time

from watchers.watcher import Watcher

try:
    # This is here to allow testing on non-raspberry pi environments
    import RPi.GPIO as GPIO
except ImportError:
    pass
import board
from adafruit_ht16k33.segments import Seg7x4, BigSeg7x4

from peripherals.indicator import Indicator
from peripherals.range_indicator import RangeIndicator
from peripherals.seven_segment_display import SevenSegmentDisplay
from peripherals.temp_sensor import TempSensor
from utils.arduino_broker import ArduinoBroker
from utils.config import Configuration
from utils.serial_broker import SerialBroker
from watchers.ambient_temp_daemon import AmbientTempDaemon
from watchers.ambient_temp_watcher import AmbientTempWatcher
from watchers.case_temp_daemon import CaseTempDaemon
from watchers.case_temp_watcher import CaseTempWatcher
from watchers.kiln_temp_daemon import KilnTempDaemon
from watchers.kiln_temp_watcher import KilnTempWatcher
from watchers.power_watcher import PowerWatcher

logger = logging.getLogger(__name__)


class KilnWatcher:
    __instance = None

    @classmethod
    def get_watcher(cls, config_file_path=None, debug_override=False):
        """
        This is the only way an instance of KilnWatcher should be retrieved, do
        not use the constructor directly. This class behaves as a singleton and
        if an instance already exists, this method will return it.
        """

        if cls.__instance:
            return cls.__instance
        else:
            cls.__instance = cls(config_file_path, debug_override)
            return cls.__instance

    def __init__(self, config_file_path=None, debug_override=False):
        self.__watching = False

        # Set the mode for the whole of the kilnwatcher
        GPIO.setmode(GPIO.BCM)

        # Load the configuration
        self.cfg = Configuration(config_file_path, debug_override)

        # Initialize the logger
        level_name = logging.INFO
        if self.cfg.get_bool(Configuration.DEBUG):
            level_name = logging.DEBUG

        logging.basicConfig(
            format="%(asctime)s %(levelname)s %(threadName)s - %(name)s - %(message)s",
            level=level_name)

        # initialize hardware communication - There should only be one instance
        # of these, globally, since the hardware only has one physical presence
        self.__arduino_broker = ArduinoBroker(SerialBroker.get_broker(self.cfg.get(Configuration.ARDUINO_UART_ADDRESS),
                                                                      self.cfg.get_int(
                                                                          Configuration.ARDUINO_UART_SPEED)),
                                              self.cfg.get_int(Configuration.ARDUINO_RESET_GPIO))
        # Reset the microcontroller before any real communication starts
        self.__arduino_broker.reset_microcontroller()

        power_sensor_serial = SerialBroker.get_broker(self.cfg.get(Configuration.POWER_SENSOR_DEVICE),
                                                      self.cfg.get_int(Configuration.POWER_SERIAL_SPEED))
        i2c = board.I2C()

        # initialize hardware intercessors - There should only be one instance
        # of these, globally, since the hardware only has one physical presence

        # Temp Sensors:
        self.ambient_temp_sensor = TempSensor(self.__arduino_broker, TempSensor.AMBIENT_TEMP)
        self.ambient_hum_sensor = TempSensor(self.__arduino_broker, TempSensor.AMBIENT_HUMIDITY)
        self.case_temp_sensor = TempSensor(self.__arduino_broker, TempSensor.CASE_TEMP)
        self.kiln_temp_sensor = TempSensor(self.__arduino_broker, TempSensor.KILN_TEMP)

        # Indicators
        self.fan_indicator = Indicator(self.__arduino_broker, Indicator.FAN)
        self.case_hot_indicator = Indicator(self.__arduino_broker, Indicator.CASE_HOT)
        self.kiln_temp_indicator = RangeIndicator(self.__arduino_broker, Indicator.KILN_HOT)

        # Seven Segment Displays
        self.kiln_temp_display = SevenSegmentDisplay(
            BigSeg7x4(i2c, address=SevenSegmentDisplay.CLOCK),
            brightness=self.cfg.get_float(Configuration.DISPLAY_BRIGHTNESS_KILN_TEMP))
        self.ambient_temp_display = SevenSegmentDisplay(
            Seg7x4(i2c, address=SevenSegmentDisplay.AMBIENT_TEMP, auto_write=False),
            brightness=self.cfg.get_float(Configuration.DISPLAY_BRIGHTNESS_AMBIENT_TEMP))
        self.case_temp_display = SevenSegmentDisplay(
            Seg7x4(i2c, address=SevenSegmentDisplay.CASE_TEMP, auto_write=False),
            brightness=self.cfg.get_float(Configuration.DISPLAY_BRIGHTNESS_CASE_TEMP))

        # Test indicators and displays
        self.fan_indicator.begin_self_test()
        self.case_hot_indicator.begin_self_test()
        self.kiln_temp_indicator.begin_self_test()
        self.kiln_temp_display.begin_self_test()
        self.ambient_temp_display.begin_self_test()
        self.case_temp_display.begin_self_test()
        time.sleep(5)
        self.fan_indicator.off()
        self.case_hot_indicator.off()
        self.kiln_temp_indicator.off()
        self.kiln_temp_display.off()
        self.ambient_temp_display.off()
        self.case_temp_display.off()

        # Create daemons
        self.__ambient_temp_daemon = AmbientTempDaemon(self.cfg, self.ambient_temp_sensor, self.ambient_temp_display,
                                                       "Ambient Temp Daemon", "Dmn_AmbT")
        self.__case_temp_daemon = CaseTempDaemon(self.cfg, self.case_temp_sensor,
                                                 self.case_hot_indicator, self.case_temp_display,
                                                 "Case Temp Daemon", "Dmn_CasT")
        self.__kiln_temp_daemon = KilnTempDaemon(self.cfg, self.kiln_temp_sensor,
                                                 self.kiln_temp_indicator, self.kiln_temp_display,
                                                 "Kiln Temp Daemon", "Dmn_KlnT")

        # Create Watchers
        self.__ambient_temp_watcher = AmbientTempWatcher(self.ambient_temp_sensor, self.ambient_hum_sensor, self.cfg,
                                                         "Ambient Temp Watcher", "Wcr_AmbT")
        self.__case_temp_watcher = CaseTempWatcher(self.case_temp_sensor, self.cfg, "Case Temp Watcher", "Wcr_CasT")
        self.__kiln_temp_watcher = KilnTempWatcher(self.kiln_temp_sensor, self.cfg, "Kiln Temp Watcher", "Wcr_Kiln")
        self.__power_watcher = PowerWatcher(power_sensor_serial, self.cfg, "Power Watcher", "Wcr_Powr")

        logger.debug("KilnWatcher Loaded. Ready to watch.")

    def your_watch_begins(self):
        """
        This is the primary watcher logic for the kiln watcher and should be
        called to bootstrap the rest of the watcher system. After bootstrapping,
        this method remains in a loop.
        """

        logger.info("Kiln Watcher starting up.")
        # Set up event monitors for fan events
        GPIO.setup(
            self.cfg.get_int(Configuration.VENT_FAN_GPIO),
            GPIO.IN,
            pull_up_down=GPIO.PUD_UP)

        # Trap ctrl c so we can clean up
        signal.signal(signal.SIGINT, self.signal_handler)

        # Start the various environmental Daemons to run indefinitely
        self.__kiln_temp_daemon.i_summon_you()
        self.__case_temp_daemon.i_summon_you()
        self.__ambient_temp_daemon.i_summon_you()

        # Wait for the fun to happen
        logger.debug("Kilnwatcher startup complete. Beginning watch...")
        self.__watch()

    def __watch(self):
        self.__watching = True
        last_fan_state = self.__fan_running()
        while self.__watching:

            if not last_fan_state == self.__fan_running():
                # Fan state changed do a thing
                if self.__fan_running():
                    logger.info("Vent Fan signal ON. Starting kiln data logging.")
                    self.__start_kiln_logging()
                else:
                    logger.info("Vent Fan signal OFF. Starting kiln data logging.")
                    self.__stop_kiln_program_logging()
            else:
                # if the fan is on, but things aren't watching, get them going
                if self.__fan_running() and not self.__all_watchers_are_watching():
                    self.__start_kiln_logging()

                # if the fan is off, but the power is still watching, stop it
                if not self.__fan_running():
                    if self.__power_watcher.watching:
                        self.__stop_kiln_program_logging()

                    # Check to see if the temp watchers are in the right state, now that the fan is off
                    self.__check_watcher_state(self.__case_temp_watcher)
                    self.__check_watcher_state(self.__kiln_temp_watcher)

                    # If nothing else is watching, turn off the ambient, otherwise, turn it on
                    if not self.__ambient_temp_watcher.watching and self.__any_watcher_is_watching():
                        self.__ambient_temp_watcher.your_watch_begins()
                    if self.__ambient_temp_watcher.watching and not self.__any_watcher_is_watching():
                        self.__ambient_temp_watcher.your_watch_has_ended()

            # update the last fan state and sleep
            last_fan_state = self.__fan_running()
            time.sleep(self.cfg.get_int(Configuration.KILN_WATCHER_POLING_INTERVAL))

    def __start_kiln_logging(self):
        if self.__all_watchers_are_watching():
            logger.debug("Received kiln START signal, but data logging is already running.")

        logger.info("Starting kiln data logging.")

        # Light the fan indicator
        self.fan_indicator.on()
        # Start the PowerWatcher
        self.__power_watcher.your_watch_begins()
        # Start the AmbientTempWatcher
        self.__ambient_temp_watcher.your_watch_begins()
        # Start the CaseTempWatcher
        self.__case_temp_watcher.your_watch_begins()
        # Start the KilnTempWatcher
        self.__kiln_temp_watcher.your_watch_begins()

    def __stop_kiln_program_logging(self):
        if not self.__power_watcher.watching:
            logger.debug("Received kiln STOP signal, but program is already stopped.")

        logger.info("Stopping kiln program data logging.")

        # Turn off the fan indicator
        self.fan_indicator.off()
        # Stop the PowerWatcher
        self.__power_watcher.your_watch_has_ended()
        # Kiln and Case Temp watchers must continue until the various temp thresholds have been reached

    @staticmethod
    def __check_watcher_state(watcher: Watcher) -> None:
        if not watcher.watching and not watcher.have_you_seen_enough():
            watcher.your_watch_begins()

        if watcher.watching and watcher.have_you_seen_enough():
            watcher.your_watch_has_ended()

    def __all_watchers_are_watching(self):
        return self.__kiln_temp_watcher.watching \
               and self.__case_temp_watcher.watching \
               and self.__ambient_temp_watcher.watching \
               and self.__power_watcher.watching

    def __any_watcher_is_watching(self):
        return self.__kiln_temp_watcher.watching \
               or self.__case_temp_watcher.watching \
               or self.__ambient_temp_watcher.watching \
               or self.__power_watcher.watching

    def __fan_running(self):
        return not GPIO.input(self.cfg.get_int(Configuration.VENT_FAN_GPIO))

    #
    # Shutdown Handler
    #
    def signal_handler(self, sig, frame):
        logger.info("Shutting down...")
        self.__watching = False
        self.__power_watcher.your_watch_has_ended()
        self.__case_temp_watcher.your_watch_has_ended()
        self.__ambient_temp_daemon.i_banish_you()
        self.__case_temp_daemon.i_banish_you()
        self.__kiln_temp_daemon.i_banish_you()
        SerialBroker.close_all()
        GPIO.cleanup()
        logger.info("fin")
        sys.exit(0)
