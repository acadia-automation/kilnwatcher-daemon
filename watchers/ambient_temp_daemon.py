# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

from peripherals.seven_segment_display import SevenSegmentDisplay
from peripherals.temp_sensor import TempSensor
from utils.config import Configuration
from watchers.environmental_daemon import EnvironmentalDaemon

logger = logging.getLogger(__name__)


class AmbientTempDaemon(EnvironmentalDaemon):
    """
    A class used to monitor the ambient temperature. See: EnvironmentalDaemon
    for a general discussion of this type of daemon.
    """

    def __init__(self, config: Configuration, ambient_temp_sensor: TempSensor,
                 ambient_temp_display: SevenSegmentDisplay, daemon_name: str, daemon_log_name: str):
        """
        param: config
        Configuration
            The configuration repository that this daemon should use for various parameters
        param: ambient_temp_sensor
        TempSensor
            The ambient sensor that should be monitored by this watcher daemon.
        param: ambient_temp_display
        SevenSegmentDisplay
            The numeric display that should show the ambient temperature and humidity.
        """
        super().__init__(config.get_int(Configuration.TEMPERATURE_MEASUREMENT_INTERVAL), daemon_name, daemon_log_name)
        self.__ambient_temp_sensor = ambient_temp_sensor
        self.__ambient_temp_display = ambient_temp_display

    def _stop_peripherals(self):
        self.__ambient_temp_display.off()

    def _single_possession(self):
        # Read the ambient temp
        ambient_temp = self.__ambient_temp_sensor.read_temperature()
        self.__ambient_temp_display.display(ambient_temp)

    def _broadcast_error(self):
        self.__ambient_temp_display.error()
