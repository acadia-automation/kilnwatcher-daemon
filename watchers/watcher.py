# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import threading
from abc import ABC, abstractmethod
from time import sleep

from typing import List, Any

from db.influx_stats_transmitter import InfluxStatsTransmitter
from utils.config import Configuration

logger = logging.getLogger(__name__)


class Watcher(ABC):

    def __init__(self, config: Configuration, watcher_name: str, watcher_log_name: str):
        self.__watching = False
        self.__watcher_thread = None
        self._config = config
        self.__watcher_name = watcher_name
        self.__watcher_log_name = watcher_log_name
        self.__measurement_interval = config.get_int(self._get_measurement_interval_config_name())

    # Thread body
    def watch(self, name):
        logger.debug(f"Watch thread started.")
        # Create resources that should exist for the duration of the watch, only
        stats_transmitter = InfluxStatsTransmitter(
            self._config.get(Configuration.INFLUX_DB_API_URL),
            self._config.get(Configuration.INFLUX_DB_BUCKET))
        resources = self._gather_watch_resources()

        while self.__watching:
            self._take_one_look(stats_transmitter, resources)
            sleep(self.__measurement_interval)

    # Thread Management
    def your_watch_begins(self):
        # Don't start this again if it is already running. It's not really safe for multiple threads.
        if self.__watching:
            logger.warning(f"{self.__watcher_name} requested to begin watch, but there is already a watch in progress.")
            return

        logger.info(f"...and now my watch begins. {self.__watcher_log_name}")
        self.__watching = True
        self.__watcher_thread = threading.Thread(target=self.watch, name=self.__watcher_log_name, args=(1,))
        self.__watcher_thread.start()

    def your_watch_has_ended(self):
        if not self.__watching:
            logger.warning(f"{self.__watcher_name} requested to end watch, but there is no watch in progress.")
            return

        self.__watching = False
        if self.__watcher_thread:
            self.__watcher_thread.join()
        logger.info("...and now my watch has ended.")

    @property
    def watching(self):
        return self.__watching

    @abstractmethod
    def _get_measurement_interval_config_name(self):
        pass

    @abstractmethod
    def _gather_watch_resources(self) -> list:
        pass

    @abstractmethod
    def _take_one_look(self, stats_transmitter: InfluxStatsTransmitter, resources: List[Any]):
        pass

    @abstractmethod
    def have_you_seen_enough(self) -> bool:
        pass
