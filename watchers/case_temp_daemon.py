# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

from peripherals.indicator import Indicator
from peripherals.seven_segment_display import SevenSegmentDisplay
from peripherals.temp_sensor import TempSensor
from utils.config import Configuration
from watchers.environmental_daemon import EnvironmentalDaemon

logger = logging.getLogger(__name__)


class CaseTempDaemon(EnvironmentalDaemon):

    def __init__(self, config: Configuration, case_temp_sensor: TempSensor, case_hot_indicator: Indicator,
                 case_temp_display: SevenSegmentDisplay, daemon_name: str, daemon_log_name: str):
        """
        Parameters
        ----------
        config : utils.config.Configuration
            The configuration repository that this daemon should use for various parameters
        case_temp_sensor : kilnwatcher.peripherals.temp_sensor.TempSensor
            The case sensor that should be monitored by this watcher daemon.
        case_hot_indicator : kilnwatcher.peripherals.indicator.Indicator
            The indicator that should be enabled when there is a high temperature warning
        case_temp_display : kilnwatcher.peripherals.seven_segment_display.SevenSegmentDisplay
            The numeric display that should show the case temperature when the temperature is over the warning threshold
        """
        super().__init__(config.get_int(Configuration.TEMPERATURE_MEASUREMENT_INTERVAL), daemon_name, daemon_log_name)
        self.__case_temp_sensor = case_temp_sensor
        self.__case_hot_indicator = case_hot_indicator
        self.__case_temp_display = case_temp_display

        self.__temp_threshold = config.get_int(Configuration.CASE_TEMP_WARNING_THRESHOLD)

    def _stop_peripherals(self):
        self.__case_hot_indicator.off()
        self.__case_temp_display.off()

    def _single_possession(self):
        # Read the kiln case temperature
        case_temp = self.__case_temp_sensor.read_temperature()

        # Check to see if temperature is above threshold
        if case_temp > self.__temp_threshold:
            # Case is hot, light indicator
            self.__case_hot_indicator.on()
            # Display current case temperature
            self.__case_temp_display.display(case_temp)
        else:
            # Case is not hot, display nothing
            self.__case_hot_indicator.off()
            # Turn off display
            self.__case_temp_display.off()

    def _broadcast_error(self):
        self.__case_hot_indicator.warning()
        self.__case_temp_display.error()
