# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
from typing import List, Any

from db.influx_stats_transmitter import InfluxStatsTransmitter
from peripherals.temp_sensor import TempSensor
from utils.config import Configuration
from utils.kilnwatcher_errors import PeripheralResponseError
from watchers.watcher import Watcher

logger = logging.getLogger(__name__)


class AmbientTempWatcher(Watcher):

    def __init__(self, ambient_temp_sensor: TempSensor, ambient_hum_sensor: TempSensor, config: Configuration,
                 watcher_name: str, watcher_log_name: str):
        super().__init__(config, watcher_name, watcher_log_name)
        self.__ambient_temp_sensor = ambient_temp_sensor
        self.__ambient_hum_sensor = ambient_hum_sensor

    def _get_measurement_interval_config_name(self):
        return Configuration.TEMPERATURE_MEASUREMENT_INTERVAL

    def _gather_watch_resources(self) -> list:
        return []

    def _take_one_look(self, stats_transmitter: InfluxStatsTransmitter, resources: List[Any]):
        try:
            ambient_temp = self.__ambient_temp_sensor.read_temperature()
            self.__transmit_metrics(ambient_temp, stats_transmitter)
        except PeripheralResponseError as sr_exception:
            logger.error("Failed to read from power sensor board.")
            logger.exception(sr_exception)

    def have_you_seen_enough(self) -> bool:
        return True

    # Utility methods
    @staticmethod
    def __transmit_metrics(temperature: int, stats_transmitter: InfluxStatsTransmitter):
        stats_transmitter.transmit(
            "environmental_temperature_sensor",
            {"environment": "indoor", "room": "garage", "area": "kiln", "sensor_type": "SHT30"},
            {"temperature": temperature})
