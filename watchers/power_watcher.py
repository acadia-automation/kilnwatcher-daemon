# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
from typing import List, Any

from peripherals.power_sensor import PowerSensor
from utils.config import Configuration
from utils.kilnwatcher_errors import SerialReadException
from utils.power_sensor_board_control import PowerSensorBoardControl
from watchers.watcher import Watcher

logger = logging.getLogger(__name__)


class PowerWatcher(Watcher):

    def __init__(self, serial_broker, config: Configuration, watcher_name: str, watcher_log_name: str):
        super().__init__(config, watcher_name, watcher_log_name)
        self.__serial_broker = serial_broker

    def _get_measurement_interval_config_name(self):
        return Configuration.POWER_SENSOR_MEASUREMENT_INTERVAL

    def _gather_watch_resources(self) -> list:
        return [PowerSensor(self.__serial_broker)]

    def _take_one_look(self, stats_transmitter, resources: List[Any]):
        # configured in _gather_watch_resources
        power_sensor = resources[0]
        try:
            metrics = power_sensor.read_power()
            PowerWatcher.__transmit_metrics(metrics, stats_transmitter)
        except (SerialReadException, UnicodeDecodeError) as sr_exception:
            logger.error("Failed to read from power sensor board.")
            logger.exception(sr_exception)
            logger.info("Attempting to reset the power sensor board")
            PowerSensorBoardControl.reset_board()

    def have_you_seen_enough(self) -> bool:
        return True

    # Utility methods
    @staticmethod
    def __transmit_metrics(power_metrics, stats_transmitter):
        # measurement, label, value, tags, time=None
        stats_transmitter.transmit(
            "current_sensor",
            PowerWatcher.__format_metadata("kiln", "1"),
            PowerWatcher.__format_field_data(power_metrics.line_1))
        stats_transmitter.transmit(
            "current_sensor",
            PowerWatcher.__format_metadata("kiln", "2"),
            PowerWatcher.__format_field_data(power_metrics.line_2))

    @staticmethod
    def __format_metadata(load, conductor):
        return {
            "load": str(load),
            "conductor": str(conductor)
        }

    @staticmethod
    def __format_field_data(single_line_metrics):
        return {
            "real_power": single_line_metrics.real_power,
            "apparent_power": single_line_metrics.apparent_power,
            "amperes_rms": single_line_metrics.amperes_rms,
            "voltage_rms": single_line_metrics.voltage_rms,
            "power_factor": single_line_metrics.power_factor
        }
