# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import yaml


class Configuration:
    # General Config
    DEBUG = "DEBUG"
    ENVIRONMENT = "ENVIRONMENT"
    # Database Configuration
    INFLUX_DB_API_URL = "INFLUX_DB_API_URL"
    INFLUX_DB_BUCKET = "INFLUX_DB_BUCKET"
    # Hardware Configuration
    VENT_FAN_GPIO = "VENT_FAN_GPIO"
    VENT_FAN_BOUNCE_TIME = "VENT_FAN_BOUNCE_TIME"
    # Arduino Configuration
    ARDUINO_RESET_GPIO = "ARDUINO_RESET_GPIO"
    ARDUINO_UART_ADDRESS = "ARDUINO_UART_ADDRESS"
    ARDUINO_UART_SPEED = "ARDUINO_UART_SPEED"
    # Power sensor Configuration
    POWER_SENSOR_DEVICE = "POWER_SENSOR_DEVICE"
    POWER_SERIAL_SPEED = "POWER_SERIAL_SPEED"
    # Power Watcher Configuration
    POWER_SENSOR_MEASUREMENT_INTERVAL = "POWER_SENSOR_MEASUREMENT_INTERVAL"
    # Temperature Daemons Configuration
    CASE_TEMP_WARNING_THRESHOLD = "CASE_TEMP_WARNING_THRESHOLD"
    KILN_TEMP_WARNING_THRESHOLD = "KILN_TEMP_WARNING_THRESHOLD"
    TEMPERATURE_MEASUREMENT_INTERVAL = "TEMPERATURE_MEASUREMENT_INTERVAL"
    # Case Temp Watcher Configuration
    TEMPERATURE_WATCH_INTERVAL = "TEMPERATURE_WATCH_INTERVAL"
    # Display configuration
    DISPLAY_BRIGHTNESS_KILN_TEMP = "DISPLAY_BRIGHTNESS_KILN_TEMP"
    DISPLAY_BRIGHTNESS_AMBIENT_TEMP = "DISPLAY_BRIGHTNESS_AMBIENT_TEMP"
    DISPLAY_BRIGHTNESS_CASE_TEMP = "DISPLAY_BRIGHTNESS_CASE_TEMP"
    # Kilnwatcher polling interval
    KILN_WATCHER_POLING_INTERVAL = "KILN_WATCHER_POLING_INTERVAL"

    def __init__(self, config_file_path=None, debug_override=False):
        self.__config = {
            Configuration.ENVIRONMENT: "test",
            Configuration.DEBUG: "False",
            Configuration.INFLUX_DB_API_URL: "http://lonelymountain.local:8086",
            Configuration.INFLUX_DB_BUCKET: "home",
            Configuration.VENT_FAN_GPIO: "26",
            Configuration.VENT_FAN_BOUNCE_TIME: "250",
            Configuration.ARDUINO_RESET_GPIO: "8",
            Configuration.ARDUINO_UART_ADDRESS: "/dev/ttyACM1",
            Configuration.ARDUINO_UART_SPEED: "115200",
            Configuration.POWER_SENSOR_DEVICE: "/dev/ttyAMA0",
            Configuration.POWER_SERIAL_SPEED: "38400",
            Configuration.POWER_SENSOR_MEASUREMENT_INTERVAL: "1",
            Configuration.CASE_TEMP_WARNING_THRESHOLD: "70",
            Configuration.KILN_TEMP_WARNING_THRESHOLD: "100",
            Configuration.TEMPERATURE_MEASUREMENT_INTERVAL: "10",
            Configuration.TEMPERATURE_WATCH_INTERVAL: "60",
            Configuration.DISPLAY_BRIGHTNESS_KILN_TEMP: "0.5",
            Configuration.DISPLAY_BRIGHTNESS_AMBIENT_TEMP: "0.75",
            Configuration.DISPLAY_BRIGHTNESS_CASE_TEMP: "0.75",
            Configuration.KILN_WATCHER_POLING_INTERVAL: "3"
        }

        # This must come AFTER the default values are initialized, otherwise,
        # the defaults will take precedence
        if config_file_path:
            self.overlay_config_file(config_file_path)

        if debug_override:
            self.__config[Configuration.DEBUG] = "True"

    def overlay_config_file(self, config_file_path):
        with open(config_file_path, "r") as yaml_file:
            file_config = yaml.load(yaml_file, Loader=yaml.FullLoader)

        # If the yaml file was empty or all comments (default scenario), don't attempt overlay
        if not file_config:
            return

        loaded_keys = file_config.keys()

        for key in self.__config.keys():
            if self.__config[key] and key in loaded_keys:
                self.__config[key] = str(file_config[key])

    def get(self, key):
        return self.__config[key]

    def get_int(self, key):
        return int(self.get(key))

    def get_float(self, key):
        return float(self.get(key))

    def get_bool(self, key):
        value = self.get(key)
        if value.lower() in ("yes", "true", "t", "1"):
            return True
        else:
            return False
