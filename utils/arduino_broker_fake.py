# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

logger = logging.getLogger(__name__)


class ArduinoBrokerFake:

    def __init__(self, temp_value):
        self.temp_value = temp_value

    def indicator_action(self, indicator_action, indicator_id, indicator_data: str = None):
        if self.temp_value:
            return True
        else:
            return False

    def read_temperature(self, sensor_id):
        return self.temp_value

    def display_temperature_value(self, display_id, temp_value):
        if self.temp_value:
            return True
        else:
            return False

    def display_off(self, display_id):
        if self.temp_value:
            return True
        else:
            return False
