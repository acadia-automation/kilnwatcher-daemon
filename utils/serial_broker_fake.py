# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

class SerialBrokerFake:

    def __init__(self, response_value):
        self.response_value = response_value
        self.request_value = None

    def request_response_from_serial(self, request_str):
        self.request_value = request_str
        return self.response_value

    def read_from_serial(self):
        return self.response_value
