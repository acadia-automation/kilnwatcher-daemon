# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import threading
import time
from typing import Optional

try:
    # This is here to allow testing on non-raspberry pi environments
    import RPi.GPIO as GPIO
except ImportError:
    pass

from utils.kilnwatcher_errors import SerialDeviceNotAvailable
from utils.serial_broker import SerialBroker

logger = logging.getLogger(__name__)


class ArduinoBroker:
    """ Manages interactions with a microcontroller using the provided Serial Broker interface. """
    # Protocol constants - These should be identical in the Arduino sketch
    REQUEST_INDICATOR_ACTION = 'i'
    REQUEST_READ_TEMP = 't'

    ACTION_ON = 'O'
    ACTION_OFF = 'F'
    ACTION_RANGE = 'R'
    ACTION_WARN = 'W'
    ACTION_TEST = 'T'

    INDICATOR_ID_FAN = 'F'
    INDICATOR_ID_CASE_HOT = 'C'
    INDICATOR_ID_KILN_HOT = 'K'
    """
    Action to light the kiln internal temperature indicator. Extra data should
    be passed to this that indicates the current temperature of the kiln.
    """

    SENSOR_CASE_TEMP = 'C'
    SENSOR_CASE_SENSOR = 'S'
    SENSOR_AMBIENT_TEMP = 'T'
    SENSOR_AMBIENT_HUM = 'H'
    SENSOR_KILN_TEMP = 'K'

    RESPONSE_SUCCESS = 'Success'

    def __init__(self, serial_broker: SerialBroker, reset_gpio: int):
        self.__ser = serial_broker
        self.__reset_gpio = reset_gpio
        self.__lock = threading.Lock()

        # Set up the reset pin
        GPIO.setup(self.__reset_gpio, GPIO.OUT, initial=GPIO.LOW)

    #
    # Data Requests
    #   These methods expect data in response to the request they send.
    #
    def read_temperature(self, sensor_id: str) -> str:
        """
        Returns the value of a temperature sensor attached to the microcontroller.

        :param sensor_id:
        str
           the sensor to read, as defined in the SENSOR_... constants of this class

        :returns:
        str
            temperature value in F for the specified sensor
        """
        logger.debug(f"Performing Read Temperature on {sensor_id}")
        # Expecting text representation of integer temp value - not checked here
        response = self.__serial_comms_with_retry(f"{ArduinoBroker.REQUEST_READ_TEMP}{sensor_id}\n")
        logger.debug(f"Received Read Temperature response: {response}")
        return response

    #
    # Action commands
    #   These methods command the Arduino to perform a simple action and return
    #   success or failure state
    #
    def indicator_action(self, indicator_action: str, indicator_id: str, indicator_data: str = None) -> bool:
        """
        Sends a request to the microcontroller to perform an action on one
        of its attached, predefined indicators.

        :param indicator_action:
        str
            the action to perform, as defined in the ACTION... constants of this class
        :param indicator_id:
        str
            the indicator where the action should be performed, as defined in the INDICATOR... constants of this class
        :param indicator_data:
        str
            additional data that affects how the action is performed, this varies based on the particular action
            and is specified in the documentation for the constants where it is relevant

        :returns:
        bool
            True if the action was successful, otherwise False
        """
        logger.debug(f"Performing Indicator action {indicator_action} on {indicator_id}")
        if indicator_data:
            command = f"{ArduinoBroker.REQUEST_INDICATOR_ACTION}{indicator_action}{indicator_id}:{indicator_data}\n"
        else:
            command = f"{ArduinoBroker.REQUEST_INDICATOR_ACTION}{indicator_action}{indicator_id}\n"
        return self.__send_and_process_simple_command(command)

    #
    # Internal communication handling methods
    #
    def __send_and_process_simple_command(self, formatted_request: str) -> bool:
        logger.debug(f"Sending request: {formatted_request}")

        response = self.__serial_comms_with_retry(formatted_request)

        if response == ArduinoBroker.RESPONSE_SUCCESS:
            return True
        else:
            logger.warning(f"Received response from Arduino of: {response}")
            return False

    def __serial_comms_with_retry(self, formatted_request: str) -> Optional[str]:
        retries = 2
        result = None
        tries = 0
        while not result and tries < retries:
            if tries > 0:
                logger.debug(f"Retrying serial interaction. {tries} failed attempts")
            try:
                tries += 1
                result = self.__ser.request_response_from_serial(formatted_request)
            except SerialDeviceNotAvailable as no_device:
                logger.debug("Serial device not available error encountered.")
                logger.exception(no_device)
                self.reset_microcontroller()

        return result

    def reset_microcontroller(self) -> None:
        """
        Sends a reset signal to the microcontroller using the provided GPIO.
        Note that after the signal is sent, there is a short wait to allow the
        board to reboot after the reset signal is sent.

        :param reset_gpio:
        int
            GPIO to use for sending the reset signal
        """
        logger.info(f"Resetting microcontroller using GPIO {self.__reset_gpio}")

        self.__ser.close()

        try:
            self.__lock.acquire()
            logger.debug("Lock acquired")
            # Set the reset GPIO to high
            GPIO.output(self.__reset_gpio, GPIO.HIGH)
            # Wait 1/2 second for reset to initiate
            time.sleep(0.5)
            # Set the reset GPIO back to low and leave it
            GPIO.output(self.__reset_gpio, GPIO.LOW)

        finally:
            if self.__lock.locked():
                self.__lock.release()
                logger.debug("Lock released")

        # Wait 5 seconds for the microcontroller to stabilize
        time.sleep(5)

        self.__ser.open()

        logger.debug("Microcontroller reset complete.")
