# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

from time import sleep

import RPi.GPIO as GPIO


class PowerSensorBoardControl:

    @staticmethod
    def reset_board():
        rst_pin = 4
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(rst_pin, GPIO.OUT)
        GPIO.output(rst_pin, GPIO.LOW)
        sleep(0.5)
        GPIO.output(rst_pin, GPIO.HIGH)
