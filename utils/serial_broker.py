# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import threading
import time

import serial

from utils.kilnwatcher_errors import SerialReadException, SerialDeviceNotAvailable

logger = logging.getLogger(__name__)


class SerialBroker:
    """
    This class handles the communication to serial devices and should be used for all serial interaction. It provides
    thread safety and caching of connections. The get_broker() method is the only means that should be used to obtain
    an instance of this class. SerialBroker connections can be closed individually or by using the close_all() method
    to close all instances that are managed by the class.

    The methods for interacting with the serial device, request_response_from_serial and read_from_serial are
    thread-safe and implement a retry logic, where they will attempt to close and reopen the serial connection for each
    retry. To turn off retry logic, set the optional retries parameter for get_broker to 1.
    """

    TEXT_ENCODING = 'utf-8'
    __instances = {}

    def __init__(self, uart_address, serial_speed, retries=3):
        self.uart_address = uart_address
        self.serial_speed = serial_speed
        self.retries = retries
        self.__lock = threading.Lock()
        self.__ser = None

    @classmethod
    def get_broker(cls, uart_address, serial_speed, retries=3):
        """
        This is the only way an instance of SerialBroker should be retrieved, do
        not use the constructor directly. This allows for thread safety and pooling
        of particular serial connections. Any call to get_broker should be paired
        with a corresponding call to close() or close_all()

        Parameters
        ----------
        uart_address : str
            The system address for the serial device
        serial_speed : int
            The speed to be used for the serial connection
        retries : int
            The number of times to retry read/write operations
        """

        if not cls.__instances.get(uart_address):
            broker_instance = cls(uart_address, serial_speed, retries)
            broker_instance.open()
            cls.__instances[uart_address] = broker_instance

        return cls.__instances.get(uart_address)

    @classmethod
    def close_all(cls):
        """
        Closes all serial connections for all instances of SerialBroker obtained from get_broker()
        """

        for key, ser in cls.__instances.items():
            ser.close()

    def request_response_from_serial(self, request_str):
        """
        Executes an operation to write a request and read a single line response from the serial device.

        Parameters
        ----------
        request_str : str
            This is the formatted message to send to the serial device

        Returns
        -------
        str
            The response from the device after the request was sent
        """
        encoded_request = request_str.encode(SerialBroker.TEXT_ENCODING)
        return self.__serial_read_op_with_retry_and_reconnect(self.__write_read, encoded_request)

    def read_from_serial(self):
        """
        Executes an operation to read a single line from the serial device.

        Returns
        -------
        str
            The data read from the device
        """
        return self.__serial_read_op_with_retry_and_reconnect(self.__read)

    def open(self):
        """
        Thread-safe way to open the serial connection to the device.
        """
        self.__serial_connect_op_with_retry(self.__open)

    def close(self):
        """
        Thread-safe way to close the serial connection to the device.
        """
        self.__serial_connect_op_with_retry(self.__close)

    def __open(self):
        if not self.__ser or not self.__ser.isOpen():
            logger.debug(f"Opening serial connection to {self.uart_address}")
            self.__ser = serial.Serial(self.uart_address, self.serial_speed, timeout=1)
            self.__ser.flush()
        else:
            logger.debug(f"Serial connection already open for {self.uart_address}.")

        return True

    def __close(self):
        if self.__ser and self.__ser.isOpen():
            logger.debug(f"Closing serial connection to {self.uart_address}")
            self.__ser.close()
        else:
            logger.debug(f"Serial connection already closed or does not exist for {self.uart_address}.")

        return True

    def __atomic_serial_operation(self, method, request=None):
        logger.debug(f"Running atomic serial operation with {method} and request {request}")
        try:
            self.__lock.acquire()
            logger.debug("Lock acquired")
            if request:
                return method(request)
            else:
                return method()
        finally:
            if self.__lock.locked():
                self.__lock.release()
                logger.debug("Lock released")

    def __serial_connect_op_with_retry(self, operation_method, request=None):
        # Attempt retries
        logger.debug(f"Running serial operation with {operation_method} and request {request}")
        result = None
        tries = 0
        while not result and tries < self.retries:
            if tries > 0:
                logger.debug(f"Retrying serial interaction. {tries} failed attempts")
            try:
                tries += 1
                result = self.__atomic_serial_operation(operation_method, request)
            except serial.SerialException as serial_error:
                logger.debug(
                    f"Encountered error interacting with serial device on port {self.uart_address}:{serial_error}")
                logger.exception(serial_error)
                time.sleep(tries / 3)
            except Exception as error:
                logger.debug("Unknown exception encountered while interacting with serial.")
                logger.exception(error)
                time.sleep(tries / 3)

        if not result:
            raise SerialReadException(
                f"Unable to perform operation on device {self.uart_address} after {self.retries} retries.")

        # Decode response, semantic processing should happen above.
        return result

    def __serial_read_op_with_retry_and_reconnect(self, operation_method, request=None):
        logger.debug(f"Running serial operation with {operation_method} and request {request}")
        response = None
        tries = 0
        while not response and tries < self.retries:
            if tries > 0:
                logger.debug(f"Retrying serial interaction. {tries} failed attempts")
            try:
                tries += 1
                response = self.__atomic_serial_operation(operation_method, request)
            except serial.serialutil.PortNotOpenError as port_error:
                logger.debug(
                    f"Encountered error with serial device not available on port {self.uart_address}:{port_error}")
                logger.exception(port_error)
                raise SerialDeviceNotAvailable(port_error)
            except serial.SerialException as serial_error:
                logger.debug(
                    f"Encountered error writing/reading serial device on port {self.uart_address}:{serial_error}")
                logger.exception(serial_error)
                self.__serial_reconnect(tries)
            except Exception as error:
                logger.debug("Unknown exception encountered while interacting with serial.")
                logger.exception(error)
                self.__serial_reconnect(tries)

        if not response:
            raise SerialReadException(
                f"Unable to read/write serial device {self.uart_address} after {self.retries} retries.")

        # Decode response, semantic processing should happen above.
        return response.decode(SerialBroker.TEXT_ENCODING).rstrip()

    def __serial_reconnect(self, sleep_factor):
        sleep_time = sleep_factor / 10
        logger.debug(f"Attempting to reconnect to device {self.uart_address} in {sleep_time} seconds.")
        time.sleep(sleep_time)
        self.close()
        self.open()

    def __write_read(self, encoded_request):
        self.__ser.write(encoded_request)
        self.__ser.flush()
        return self.__ser.readline()

    def __read(self):
        return self.__ser.readline()
