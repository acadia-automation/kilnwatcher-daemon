# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

class PeripheralResponseError(Exception):
    """ Raised when a response from a sensor or other peripheral does not
    conform to the expected input """

    def __init__(self, message, response):
        self.message = message
        self.response = response


class SerialReadException(Exception):
    """ Raised when no response is received from serial device """


class SerialDeviceNotAvailable(Exception):
    """ Raised when the port for the serial device does not exist """
