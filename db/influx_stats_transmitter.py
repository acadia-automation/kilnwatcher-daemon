# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS
from urllib3 import Retry

logger = logging.getLogger(__name__)


class InfluxStatsTransmitter:
    """
    Transmits data to an influx database rest api.
    """

    def __init__(self, database_api_url, bucket):
        self.__database_api_url = database_api_url
        self.__bucket = bucket
        # Token and Org are requirements for the 2.x version of influxdb, for 1.8, just pass
        # junk values. Update this with real values in the future, if 2.x becomes an option
        # for RPi installation. Currently, it is not, as Raspbian 64 bit is still in Beta.
        self.__token = "ignored"
        self.__org = ""

        retries = Retry(connect=5, read=2, redirect=5)
        client = InfluxDBClient(url=database_api_url, token=self.__token, org=self.__org, retries=retries)
        self.__write_api = client.write_api(write_options=SYNCHRONOUS)

    # Tags and fields should be a dictionary of name value pairs
    def transmit(self, measurement, tags, fields, time=None):
        point = self.__generate_data_point(measurement, tags, fields, time)
        logger.debug(f"Data Point: {point}")

        self.__write_api.write(bucket=self.__bucket, record=point)

    # Tags and fields should be a dictionary of name value pairs
    @staticmethod
    def __generate_data_point(measurement, tags, fields, time=None):
        p = Point(measurement)
        for tag_key, tag_value in tags.items():
            p.tag(tag_key, tag_value)
        for field_key, field_value in fields.items():
            p.field(field_key, field_value)
        if time:
            p.time(time)

        return p
