# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

class StatsTransmitterFakeStdout:

    def __init__(self):
        print("Fake StatsTransmitter Initialized")

    # Tags and fields should be a dictionary of name value pairs
    @staticmethod
    def transmit(measurement, tags, fields, time=None):
        tag_packet = ','.join(map('='.join, tags.items()))
        field_packet = ','.join(map('='.join, fields.items()))
        packet = measurement + "," + tag_packet + " " + field_packet
        if time:
            packet += " " + time.timestamp()

        print("Transmitting:" + packet)
