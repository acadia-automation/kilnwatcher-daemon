#!/usr/bin/env python3

# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import getopt
import sys
from watchers.kiln_watcher import KilnWatcher


#
# Command Line Interface for KilnWatcher
#
def usage():
    print("kiln_watcher.py [option]")
    print("  -h --help          : show this message")
    print("  -d --debug         : run with debug level logging")
    print("  -c --config <file> : use the configuration file specified")


def main(argv):
    config = None
    debug = False

    try:
        opts, args = getopt.getopt(argv, "c:dh", ["debug", "config=", "help"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-c", "--config"):
            config = arg
            print(f"Kilnwatcher starting with config file: {config}", flush=True)
        elif opt in ("-d", "--debug"):
            debug = True
            print("Kilnwatcher starting in debug override mode", flush=True)
        elif opt in ("-h", "--help"):
            usage()
            sys.exit()

    kiln_watcher = KilnWatcher.get_watcher(config_file_path=config, debug_override=debug)
    kiln_watcher.your_watch_begins()


if __name__ == "__main__":
    main(sys.argv[1:])
