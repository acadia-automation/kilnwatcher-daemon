#!/bin/bash

# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

# Source locations
# REVIEW BEFORE INSTALL: Set this appropriately
TEMP_DIR="/tmp"
CURL="/usr/bin/curl"
TAR="/usr/bin/tar"
# These shouldn't change unless the source repository does
INSTALL_ARCHIVE_BASE_URL="http://40newthings.com/kilnwatcher"
CURL_OPTS="-fsSL --output"
INSTALL_ARCHIVE_DAEMON="kilnwatcher-daemon.tar.gz"
KILNWATCHER_ROOT="kilnwatcher"

# Install archive sub-paths
SRC_CONFIG_DIR="config"


# Target locations
# REVIEW BEFORE INSTALL: Set these appropriately
INSTALL_LOC="/usr/local"
TRGT_DAEMON_DIR="src"
TRGT_ARCHIVE_DIR="src/archive"
TRGT_CONFIG_DIR="etc"
# These shouldn't change unless the source repository does
PIPFILE_FILE="Pipfile"
CONFIG_FILE="kilnwatcher_config.yaml"


# precompile some useful variable
starting_dir=$PWD
datestr=`date +"%Y%m%d.%H.%M.%S"`

download_command="${CURL} ${CURL_OPTS} ${TEMP_DIR}/${INSTALL_ARCHIVE_DAEMON} ${INSTALL_ARCHIVE_BASE_URL}/${INSTALL_ARCHIVE_DAEMON}"

install_source_path="${TEMP_DIR}/${KILNWATCHER_ROOT}"

kilnwatcher_source_path="${install_source_path}"
kilnwatcher_target_path="${INSTALL_LOC}/${TRGT_DAEMON_DIR}/${KILNWATCHER_ROOT}"
kilnwatcher_archive_dir="${INSTALL_LOC}/${TRGT_ARCHIVE_DIR}"
kilnwatcher_archive_path="${kilnwatcher_archive_dir}/${KILNWATCHER_ROOT}_${datestr}"

pipfile_source_path="${install_source_path}/${PIPFILE_FILE}"
pipfile_target_path="${kilnwatcher_target_path}/${PIPFILE_FILE}"

config_source_path="${install_source_path}/${SRC_CONFIG_DIR}/${CONFIG_FILE}"
config_target_dir="${INSTALL_LOC}/${TRGT_CONFIG_DIR}/${KILNWATCHER_ROOT}"
config_target_path="${config_target_dir}/${CONFIG_FILE}"


echo ""
echo "Fetching Kilnwatcher Daemon install files..."

# download the installation archive to tmp
${download_command}

# unpack the install archive
if [[ -d "${install_source_path}" ]]; then
  echo "    -- Previous Kilnwatcher installation left install files. Overwriting..."
else
  mkdir ${install_source_path}
  echo "    -- Created install directory: ${install_source_path}"
fi

cd ${install_source_path}
${TAR} -zxf ${TEMP_DIR}/${INSTALL_ARCHIVE_DAEMON}


echo ""
echo "Installing Kilnwatcher service files..."
echo ""

# Check to see if the python dependencies were updated
UPDATE_PYTHON_DEPENDENCIES=1
if [[ -f "${pipfile_target_path}" ]] && diff ${pipfile_target_path} ${pipfile_source_path} > /dev/null
then
  UPDATE_PYTHON_DEPENDENCIES=0
  echo "Pipfile hasn't changed. Python dependencies will NOT be updated."
  echo ""
fi

# Install kilnwatcher software locally
if [[ -d "${kilnwatcher_target_path}" ]]; then
  echo "Previous Kilnwatcher installation exists."
  if [[ ! -d "${kilnwatcher_archive_dir}" ]]; then
    sudo mkdir ${kilnwatcher_archive_dir}
    echo "    -- Created archive directory: ${kilnwatcher_archive_dir}"
  fi
  sudo mv ${kilnwatcher_target_path} ${kilnwatcher_archive_path}
  echo "    -- Moved previous to ${kilnwatcher_archive_path}"
else
  echo "Kilnwatcher directory ${kilnwatcher_target_path} does not exist."
  echo "    -- No previous version to archive"
fi
sudo cp -r ${kilnwatcher_source_path} ${kilnwatcher_target_path}
echo "    -- Installed Kilnwatcher to ${kilnwatcher_target_path}"
echo ""

# Install kilnwatcher config file if it's not already there
if [[ -f "${config_target_path}" ]]; then
  echo "Kilnwatcher config file already exists."
  echo "    -- No update to ${config_target_path}"
else
  echo "Kilnwatcher config file does not exist."
  if [[ ! -f "${config_target_dir}" ]]; then
    sudo mkdir ${config_target_dir}
    echo "    -- Created config directory: ${config_target_dir}"
  fi
  sudo cp ${config_source_path} ${config_target_path}
  echo "    -- Installed config: ${config_target_path}"
fi
echo ""


# Install python dependencies
if [[ "${UPDATE_PYTHON_DEPENDENCIES}" -eq 1 ]]; then
  echo "Installing/updating python dependencies..."
  cd ${kilnwatcher_target_path}
  sudo pipenv install --ignore-pipfile
  cd ${starting_dir}
  echo ""
fi


# Install python dependencies
echo "Removing installation files..."
rm -Rf ${install_source_path}
rm ${TEMP_DIR}/${INSTALL_ARCHIVE_DAEMON}


echo "Installation complete."
echo ""
