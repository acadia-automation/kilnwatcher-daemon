# kilnwatcher - Kiln Watcher

What your project does
* Tracks power usage and temperatures for a pottery kiln in the garage.
* Submits data to a remote time series database (Influx)
* WIP to get v1 daemon in place, approaching production ready code

How to install it
Example usage
How to set up the dev environment
How to ship a change
Change log
License and author info

Replacement docs below:

## Kiln Watcher
The Kiln Watcher is software side of device that tracks various metrics related
to the firing of a pottery kiln. Some metrics are displayed directly on the
Kiln Watcher device, primarily, these function as immediate information for
someone approaching the kiln and are intended as additional safety precautions.
All metrics are sent to a time series database (Influx) for inspection and
analysis during and after the firing.

## Motivation
Mostly this was created out of my curiosity for the behavior and power
consumption of my electric pottery kiln. It also functions as additional safety
equipment to indicate clearly when the kiln is hot and may be dangerous to touch
or approach.

## Build status
Currently, there is no build process for this project, it can be installed and
run by cloning this repository to the target Raspberry Pi environment. Code is
included to compile and load onto a suitable Arduino type board.

## Code style
* numpy style doc comments

## In Pictures
![40 New Things](https://40newthings.com/img/main/logo.png "40 New Things")

## Physical equipment used
* TODO: Raspberry Pi Zero
* TODO: Adafruit [Trinket M0](http://adafruit.com)
* TODO: Lechacal [3v1 Power sensor board](http://lechacal.com)
* TODO: split core transformers
* TODO: Adafruit [Infrared Temperature sensor]
* TODO: Adafruit [Neopixel Jewel]
* TODO: Adafruit [7 Segment display with Backpack]
* TODO: Adafruit [Proto boards]
* TODO: Optocouplers

## Features
[What makes your project stand out?]

## Installation
1. Clone the kilnwatcher repository to the target system
2. Review the ha/kilnwatcher/install/bin/install_kilnwatcher.bash file and edit
   global path information as needed.
3. From the kilnwatcher bin directory, execute the install file with bash:
   ~> /bin/bash ./install_kilnwatcher.bash

## Tests
[Describe and show how to run the tests with code examples.]

## How to use?
[If people like your project they’ll want to learn how they can use it. To do so include step by step guide to use your project.]

## Contribute
[Let people know how they can contribute into your project. A [contributing guideline](https://github.com/zulip/zulip-electron/blob/master/CONTRIBUTING.md) will be a big plus.]

## Credits
???

#### Anything else that seems useful

## License
???
