#!/usr/bin/env python3

# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import unittest

import peripherals.seven_segment_display
from test.seven_segment_display_fake import SevenSegmentDisplayFake

logger = logging.getLogger(__name__)


class TestSevenSegmentDisplayRPi(unittest.TestCase):

    def test_display_off(self):
        seven_segment_success = SevenSegmentDisplayFake(True)
        test_display = peripherals.seven_segment_display.SevenSegmentDisplay(seven_segment_success)

        test_display.display("1234")
        self.assertTrue(test_display.display_on, "Display should be in 'on' status")

        self.assertTrue(test_display.off(), "Display should turn off successfully")
        self.assertFalse(test_display.display_on, "Display should be in 'off' status")

    def test_display_set_value_valid(self):
        test_input_value = "100"
        test_display_value = " 100"
        seven_segment_success = SevenSegmentDisplayFake(True)
        test_display = peripherals.seven_segment_display.SevenSegmentDisplay(seven_segment_success)

        self.assertTrue(test_display.set_value(test_input_value), f"Display should set to {test_input_value}")
        self.assertEqual(test_display.current_value, test_display_value,
                         f"Display should be set to {test_display_value}")

    def test_display_set_value_invalid_too_long(self):
        test_value = "10000"
        seven_segment_success = SevenSegmentDisplayFake(True)
        test_display = peripherals.seven_segment_display.SevenSegmentDisplay(seven_segment_success)

        self.assertFalse(test_display.set_value(test_value), f"Display should fail to set to {test_value}")
        self.assertFalse(test_display.current_value, "Display should evaluate to False")

    def test_display_set_value_invalid_non_numeric(self):
        test_input_value = "ABC"
        seven_segment_success = SevenSegmentDisplayFake(True)
        test_display = peripherals.seven_segment_display.SevenSegmentDisplay(seven_segment_success)

        self.assertFalse(test_display.set_value(test_input_value), f"Display should fail to set to {test_input_value}")
        self.assertFalse(test_display.current_value, "Display should evaluate to False")

    def test_display_set_value_invalid_none(self):
        test_value = None
        seven_segment_success = SevenSegmentDisplayFake(True)
        test_display = peripherals.seven_segment_display.SevenSegmentDisplay(seven_segment_success)

        self.assertFalse(test_display.set_value(test_value), f"Display should fail to set to {test_value}")
        self.assertFalse(test_display.current_value, "Display should evaluate to False")

    def test_display_display_valid(self):
        test_input_value = "200"
        test_display_value = " 200"
        seven_segment_success = SevenSegmentDisplayFake(True)
        test_display = peripherals.seven_segment_display.SevenSegmentDisplay(seven_segment_success)

        self.assertTrue(test_display.display(test_input_value), f"Display should be set to {test_input_value}")
        self.assertEqual(test_display.current_value, test_display_value,
                         f"Display should be set to {test_display_value}")
        self.assertTrue(test_display.display_on, "Display should be set to 'on' status")

    def test_display_display_valid_clock(self):
        test_input_value = "03:12"
        test_display_value = " 3:12"
        seven_segment_success = SevenSegmentDisplayFake(True)
        test_display = peripherals.seven_segment_display.SevenSegmentDisplay(seven_segment_success)

        self.assertTrue(test_display.display(test_input_value), f"Display should be set to {test_input_value}")
        self.assertEqual(test_display.current_value, test_display_value,
                         f"Display should be set to {test_display_value}")
        self.assertTrue(test_display.display_on, "Display should be set to 'on' status")

    def test_display_display_invalid(self):
        test_value = "ABC"
        seven_segment_success = SevenSegmentDisplayFake(True)
        test_display = peripherals.seven_segment_display.SevenSegmentDisplay(seven_segment_success)

        self.assertFalse(test_display.display(test_value), f"Display should fail to set to {test_value}")
        self.assertFalse(test_display.current_value, "Display should evaluate to False")
        self.assertFalse(test_display.display_on, "Display should be set to 'off' status")

    def test_display_display_invalid_clock(self):
        test_value = "03:A9"
        seven_segment_success = SevenSegmentDisplayFake(True)
        test_display = peripherals.seven_segment_display.SevenSegmentDisplay(seven_segment_success)

        self.assertFalse(test_display.display(test_value), f"Display should fail to set to {test_value}")
        self.assertFalse(test_display.current_value, "Display should evaluate to False")
        self.assertFalse(test_display.display_on, "Display should be set to 'off' status")


# TODO: Think about the exception case here.

if __name__ == '__main__':
    unittest.main()
