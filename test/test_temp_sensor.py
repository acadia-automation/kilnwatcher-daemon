#!/usr/bin/env python3

# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import unittest
import logging
import utils.arduino_broker
import utils.arduino_broker_fake
import utils.kilnwatcher_errors
import peripherals.temp_sensor

logger = logging.getLogger(__name__)


class TestTempSensor(unittest.TestCase):

    def test_read_temperature_success_100(self):
        test_temp_sensor = peripherals.temp_sensor.TempSensor(
            utils.arduino_broker_fake.ArduinoBrokerFake("100"),
            utils.arduino_broker.ArduinoBroker.SENSOR_CASE_TEMP)

        self.assertEqual(test_temp_sensor.read_temperature(), 100, "Sensor should return specified value.")

    def test_read_temperature_fail_none(self):
        test_temp_sensor = peripherals.temp_sensor.TempSensor(
            utils.arduino_broker_fake.ArduinoBrokerFake(None),
            utils.arduino_broker.ArduinoBroker.SENSOR_CASE_TEMP)

        self.assertRaises(utils.kilnwatcher_errors.PeripheralResponseError, test_temp_sensor.read_temperature)

    def test_read_temperature_fail_ABCD(self):
        test_temp_sensor = peripherals.temp_sensor.TempSensor(
            utils.arduino_broker_fake.ArduinoBrokerFake("ABCD"),
            utils.arduino_broker.ArduinoBroker.SENSOR_CASE_TEMP)

        self.assertRaises(utils.kilnwatcher_errors.PeripheralResponseError, test_temp_sensor.read_temperature)


if __name__ == '__main__':
    unittest.main()
