#!/usr/bin/env python3

# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import unittest

import peripherals.power_sensor
import utils.serial_broker_fake

logger = logging.getLogger(__name__)


class TestPowerSensor(unittest.TestCase):

    def test_read_power_success(self):
        serial_broker = utils.serial_broker_fake.SerialBrokerFake("11 1.1 2.2 3.3 4.4 5.5 6.6 7.7 8.8 9.9 10.0")
        power_sensor = peripherals.power_sensor.PowerSensor(serial_broker)

        metrics = power_sensor.read_power()
        self.assertIsInstance(metrics, peripherals.power_sensor.PowerMetrics)

    def test_read_power_failure_too_few_values(self):
        serial_broker = utils.serial_broker_fake.SerialBrokerFake("11 1.1 2.2 3.3 4.4 5.5 6.6")
        power_sensor = peripherals.power_sensor.PowerSensor(serial_broker)

        self.assertRaises(IndexError, power_sensor.read_power)

    def test_read_power_success_line_values(self):
        serial_broker = utils.serial_broker_fake.SerialBrokerFake("11 1.1 1.1 1.1 1.1 1.1 2.1 2.1 2.1 2.1 2.1")
        power_sensor = peripherals.power_sensor.PowerSensor(serial_broker)

        metrics = power_sensor.read_power()
        self.assertEqual(metrics.line_1.real_power, 1.1, "Line 1 value should be int 1.1")
        self.assertEqual(metrics.line_1.apparent_power, 1.1, "Line 1 value should be int 1.1")
        self.assertEqual(metrics.line_1.amperes_rms, 1.1, "Line 1 value should be int 1.1")
        self.assertEqual(metrics.line_1.voltage_rms, 1.1, "Line 1 value should be int 1.1")
        self.assertEqual(metrics.line_1.power_factor, 1.1, "Line 1 value should be int 1.1")
        self.assertEqual(metrics.line_2.real_power, 2.1, "Line 2 value should be int 2.1")
        self.assertEqual(metrics.line_2.apparent_power, 2.1, "Line 2 value should be int 2.1")
        self.assertEqual(metrics.line_2.amperes_rms, 2.1, "Line 2 value should be int 2.1")
        self.assertEqual(metrics.line_2.voltage_rms, 2.1, "Line 2 value should be int 2.1")
        self.assertEqual(metrics.line_2.power_factor, 2.1, "Line 2 value should be int 2.1")


if __name__ == '__main__':
    unittest.main()
