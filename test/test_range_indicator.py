#!/usr/bin/env python3

# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import unittest
import logging

import peripherals.range_indicator
import utils.arduino_broker_fake

logger = logging.getLogger(__name__)


class TestRangeIndicator(unittest.TestCase):

    def test_indicator_on_success(self):
        arduino_broker = utils.arduino_broker_fake.ArduinoBrokerFake(True)
        test_indicator = peripherals.range_indicator.RangeIndicator(arduino_broker, 14)
        self.assertTrue(test_indicator.on(), "Indicator should turn on successfully")
        self.assertTrue(test_indicator.indicator_on, "Indicator should be in 'on' status")

    def test_indicator_on_failure(self):
        arduino_broker = utils.arduino_broker_fake.ArduinoBrokerFake(False)
        test_indicator = peripherals.range_indicator.RangeIndicator(arduino_broker, 14)
        self.assertFalse(test_indicator.on(), "Indicator should fail to turn on")
        self.assertFalse(test_indicator.indicator_on, "Indicator should be in 'off' status")

    def test_indicator_range_success(self):
        arduino_broker = utils.arduino_broker_fake.ArduinoBrokerFake(True)
        test_indicator = peripherals.range_indicator.RangeIndicator(arduino_broker, 14)
        self.assertTrue(test_indicator.range_on(300), "Indicator should turn on successfully")
        self.assertTrue(test_indicator.indicator_on, "Indicator should be in 'on' status")

    def test_indicator_range_failure_no_range_provided(self):
        arduino_broker = utils.arduino_broker_fake.ArduinoBrokerFake(True)
        test_indicator = peripherals.range_indicator.RangeIndicator(arduino_broker, 14)
        self.assertFalse(test_indicator.range_on("Invalid Int"), "Indicator should fail to turn on")
        self.assertFalse(test_indicator.indicator_on, "Indicator should be in 'off' status")

    def test_indicator_range_failure_arduino_failure(self):
        arduino_broker = utils.arduino_broker_fake.ArduinoBrokerFake(False)
        test_indicator = peripherals.range_indicator.RangeIndicator(arduino_broker, 14)
        self.assertFalse(test_indicator.range_on(300), "Indicator should turn on successfully")
        self.assertFalse(test_indicator.indicator_on, "Indicator should be in 'on' status")

    def test_indicator_off(self):
        arduino_broker = utils.arduino_broker_fake.ArduinoBrokerFake(True)
        test_indicator = peripherals.range_indicator.RangeIndicator(arduino_broker, 14)
        self.assertTrue(test_indicator.off(), "Indicator should turn off successfully")
        self.assertFalse(test_indicator.indicator_on, "Indicator should be in 'off' status")


if __name__ == '__main__':
    unittest.main()
