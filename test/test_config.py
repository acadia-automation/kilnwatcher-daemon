#!/usr/bin/env python3

# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import unittest
import logging

import utils.config

logger = logging.getLogger(__name__)


class TestConfiguration(unittest.TestCase):

    def test_config_success_string(self):
        key = utils.config.Configuration.DEBUG
        cfg = utils.config.Configuration()

        cfg_value = cfg.get(key)
        self.assertIsNotNone(cfg_value, "Value should be returned for {key}")
        self.assertIsInstance(cfg_value, str, "Value should type 'str'")

    def test_config_success_int(self):
        key = utils.config.Configuration.VENT_FAN_GPIO
        cfg = utils.config.Configuration()

        cfg_value = cfg.get_int(key)
        self.assertIsNotNone(cfg_value, "Value should be returned for {key}")
        self.assertIsInstance(cfg_value, int, "Value should be type 'int'")

    def test_config_success_float(self):
        key = utils.config.Configuration.DISPLAY_BRIGHTNESS_KILN_TEMP
        value = 0.5
        cfg = utils.config.Configuration()

        cfg_value = cfg.get_float(key)
        self.assertIsNotNone(cfg_value, "Value should be returned for {key}")
        self.assertIsInstance(cfg_value, float, "Value should be type 'float'")
        self.assertEqual(value, cfg_value, "Values should be equal")

    def test_config_success_boolean(self):
        key = utils.config.Configuration.DEBUG
        cfg = utils.config.Configuration()

        cfg_value = cfg.get_bool(key)
        self.assertIsNotNone(cfg_value, "Value should be returned for {key}")
        self.assertIsInstance(cfg_value, bool, "Value should be type 'bool'")

    def test_config_success_debug_override(self):
        key = utils.config.Configuration.DEBUG
        cfg = utils.config.Configuration(debug_override=True)

        cfg_value = cfg.get_bool(key)
        self.assertTrue(cfg_value, "Debug should override to True")

    def test_config_fail_invalid_key(self):
        key = "Invalid Configuration Key"
        cfg = utils.config.Configuration()

        self.assertRaises(KeyError, cfg.get, key)

    def test_config_success_file_overlay(self):
        key = utils.config.Configuration.ARDUINO_UART_ADDRESS
        test_config_file_path = "./test/config_file_test.yaml"
        overlay_value = "THIS_TEST_VALUE"
        cfg = utils.config.Configuration(test_config_file_path)

        self.assertEqual(cfg.get(key), overlay_value, "File load did not overlay default with new value")

    def test_config_success_blank_file_overlay(self):
        key = utils.config.Configuration.DEBUG
        test_config_file_path = "./test/config_file_test_all_comments.yaml"
        cfg = utils.config.Configuration(test_config_file_path)

        self.assertEqual(cfg.get(key), "False", "File should not have overlay default with new value")

    def test_config_fail_file_overlay(self):
        key = utils.config.Configuration.INFLUX_DB_API_URL
        test_config_file_path = "./test/config_file_test.yaml"
        overlay_value = "NEVER_THIS_TEST_VALUE"
        cfg = utils.config.Configuration(test_config_file_path)

        self.assertEqual(cfg.get(key), overlay_value, "File load did not overlay default with new value")


if __name__ == '__main__':
    unittest.main()
