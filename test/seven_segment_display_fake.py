# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

logger = logging.getLogger(__name__)


class SevenSegmentDisplayFake:

    def __init__(self, success):
        self.success = success

        self.brightness = 1
        self.display_value = None
        self.fill_value = 1
        self.__digits = {}
        self.colon = False

    def print(self, value):
        if self.success:
            self.display_value = value

    def set_digit_raw(self, digit, value):
        self.__digits = {digit, value}

    def fill(self, value):
        self.fill_value = value

    def show(self):
        pass
