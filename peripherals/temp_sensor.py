# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

from utils.arduino_broker import ArduinoBroker
from utils.kilnwatcher_errors import PeripheralResponseError


class TempSensor:

    CASE_TEMP = 1
    CASE_TEMP_SENSOR_BODY = 2
    AMBIENT_TEMP = 3
    AMBIENT_HUMIDITY = 4
    KILN_TEMP = 5

    __arduino_sensor_id = None

    def __init__(self, arduino_broker, sensor_id):
        self.__arduino_broker = arduino_broker
        self.sensor_id = sensor_id

        if self.sensor_id == TempSensor.CASE_TEMP:
            self.__arduino_sensor_id = ArduinoBroker.SENSOR_CASE_TEMP
        if self.sensor_id == TempSensor.CASE_TEMP_SENSOR_BODY:
            self.__arduino_sensor_id = ArduinoBroker.SENSOR_CASE_TEMP
        if self.sensor_id == TempSensor.AMBIENT_TEMP:
            self.__arduino_sensor_id = ArduinoBroker.SENSOR_AMBIENT_TEMP
        if self.sensor_id == TempSensor.AMBIENT_HUMIDITY:
            self.__arduino_sensor_id = ArduinoBroker.SENSOR_AMBIENT_HUM
        if self.sensor_id == TempSensor.KILN_TEMP:
            self.__arduino_sensor_id = ArduinoBroker.SENSOR_KILN_TEMP

    def read_temperature(self):
        temp_response = self.__arduino_broker.read_temperature(self.__arduino_sensor_id)

        if temp_response and temp_response.isnumeric():
            return int(temp_response)
        else:
            raise PeripheralResponseError("Non-numeric or empty response received", temp_response)
