# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

logger = logging.getLogger(__name__)


class PowerSensor:
    """
    This class is an interface to a power usage sensor board connected over
    serial interface. In particular, this is written to interface with a board
    from Lechacal (http://lechacal.com) that has two power probes connected. An
    instance of this class represents a single physical indicator.
    """
    def __init__(self, serial_broker):
        self.__serial_broker = serial_broker

    def read_power(self):
        """
        Takes a power reading from the sensor board and parses the data into a
        structured, data object.

        returns:
        PowerMetrics
            object representation of the power reading data
        """
        readings = self.__serial_broker.read_from_serial()

        logger.debug(f"Power readings, raw string (between pipes) |{readings}|")

        # Create an array of the data
        data = readings.split()

        line_1_metrics = SingleLinePowerMetrics(data[1], data[2], data[3], data[4], data[5])
        line_2_metrics = SingleLinePowerMetrics(data[6], data[7], data[8], data[9], data[10])

        return PowerMetrics(line_1_metrics, line_2_metrics)


class PowerMetrics:
    """
    Represents the collected power metrics that are read from the power sensing
    board. Currently, it is written to read data for two monitored power loads.
    This class is really a container for SingleLinePowerMetrics.
    """
    def __init__(self, line1_metrics, line2_metrics):
        self.line_1 = line1_metrics
        self.line_2 = line2_metrics

    def __str__(self):
        return f'{self.__class__.__name__} Line1:{self.line_1.real_power}W, Line2:{self.line_2.real_power}W'

    def __repr__(self):
        return f'{self.__class__.__name__}({self.line_1!r}, {self.line_2!r}'


class SingleLinePowerMetrics:
    """
    Represents the power metrics that are read from the power sensing board for
    a single load.
    """
    def __init__(self, real_power, apparent_power, amperes_rms, voltage_rms, power_factor):
        self.real_power = float(real_power)
        self.apparent_power = float(apparent_power)
        self.amperes_rms = float(amperes_rms)
        self.voltage_rms = float(voltage_rms)
        self.power_factor = float(power_factor)

    def __str__(self):
        return f'SingleLinePowerMetrics power:{self.real_power}W'

    def __repr__(self):
        return f'{self.__class__.__name__}({self.real_power!r}, {self.apparent_power!r}, {self.amperes_rms!r}, {self.voltage_rms!r}, {self.power_factor!r}'
