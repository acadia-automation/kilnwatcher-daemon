# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

from peripherals.indicator import Indicator
from utils.arduino_broker import ArduinoBroker

logger = logging.getLogger(__name__)


class RangeIndicator(Indicator):
    """
    This extends the base Indicator class to add a specific behavior to the
    indicator for conveying information about data in a range (in this case, it
    is mostly likely a temperature within a known range - kiln temp.
    """
    def range_on(self, value: int) -> bool:
        """
        Turns on the indicator and requests that it display range information
        as well (shift color, blink rate, etc)

        param: value
        int
            value to set the indicator to within its set range
        """
        logger.debug(f"Indicator {self.indicator_id} turning on with range data {value}")
        if isinstance(value, int):
            result = self.arduino_broker.indicator_action(ArduinoBroker.ACTION_RANGE, self._arduino_indicator_id,
                                                          indicator_data=str(value))
        else:
            logger.error("Received non-integer value for range data, failing.")
            result = False

        if result:
            self.indicator_on = True
        else:
            logger.warning(f"Failed to set indicator {self.indicator_id} to ON with range value {value}")

        return result
