# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import re

logger = logging.getLogger(__name__)


class SevenSegmentDisplay:
    """
    This class is for using the I2C from the RPi board directly. To use the I2C
    from the arduino board via serial communication from the RPi, use the other
    seven segment display class
    """

    AMBIENT_TEMP = 0X72
    CASE_TEMP = 0x70
    CLOCK = 0x71

    def __init__(self, display, brightness: int = 1):
        self.__display = display
        self.__display.brightness = brightness

        self.display_on = False
        self.current_value: str = ""

    def display(self, value=None) -> bool:
        if value:
            self.set_value(value)

        if self.current_value:
            try:
                # The colon has a problem where it doesn't turn off on its own
                # when you display a string with no colon in it
                if self.__display.colon and ":" not in self.current_value:
                    self.__display.colon = False

                self.__display.print(self.current_value)
                self.__display.show()
                self.display_on = True
                return True
            except OSError as ose:
                logger.error("Encountered error writing to display")
                logger.exception(ose)
        else:
            self.off()

        return False

    def set_value(self, value) -> bool:
        string_value = str(value)
        # if it looks like a number of an appropriate length
        if len(string_value) <= 4 and string_value.isnumeric():
            if isinstance(value, int):
                self.current_value = "{:4d}".format(value)
            else:
                self.current_value = "{:>4}".format(value)
        # Or if it looks like a clock time formatted with a colon
        elif len(string_value) == 5 and re.fullmatch('\d\d:\d\d', string_value):
            if value.startswith('0'):
                self.current_value = " " + value[1:]
            else:
                self.current_value = value
        else:
            if len(string_value) > 30:
                string_value = string_value[30] + "..."
            logger.error(
                f"Display {self.__display} was given an invalid value, {string_value}. value must be numeric and 4 or fewer characters (int between 0-9999) or a 5 digit time string (MM:SS)")
            return False

        logger.debug(f"SevenSegmentDisplay {self.__display} value set to {self.current_value}")
        return True

    def off(self) -> bool:
        logger.debug(f"SevenSegmentDisplay {self.__display} turning off")
        self.__display.fill(False)
        self.__display.show()
        self.display_on = False
        return True

    def begin_self_test(self):
        logger.debug(f"SevenSegmentDisplay {self.__display} starting self test")
        self.__display.marquee('0123456789', 0.1, False)
        self.__display.show()
        self.display_on = True

    def error(self):
        # Set display to show "Error"
        logger.error(f"Displaying Error message on {self.__display}.")
        self.__display.set_digit_raw(0, 0b01111001)
        self.__display.set_digit_raw(1, 0b01010000)
        self.__display.set_digit_raw(2, 0b01010000)
        self.__display.set_digit_raw(3, 0b01011100)
        self.__display.show()
        self.display_on = True
