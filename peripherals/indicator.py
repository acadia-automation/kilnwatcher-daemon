# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging

from utils.arduino_broker import ArduinoBroker

logger = logging.getLogger(__name__)


class Indicator:
    """
    This class is an interface to indicator lights that are managed by a micro-
    controller attached to the system. Available lights are enumerated as
    constants in this class. An instance of this class represents a single
    physical indicator.
    """
    FAN = 'F'
    CASE_HOT = 'C'
    KILN_HOT = 'K'

    def __init__(self, arduino_broker, indicator_id):
        self.indicator_on = False
        self.arduino_broker = arduino_broker
        self.indicator_id = indicator_id

        self._arduino_indicator_id = None
        if indicator_id == Indicator.FAN:
            self._arduino_indicator_id = ArduinoBroker.INDICATOR_ID_FAN
        if indicator_id == Indicator.CASE_HOT:
            self._arduino_indicator_id = ArduinoBroker.INDICATOR_ID_CASE_HOT
        if indicator_id == Indicator.KILN_HOT:
            self._arduino_indicator_id = ArduinoBroker.INDICATOR_ID_KILN_HOT

    def on(self):
        """
        Turns the indicator on.

        returns:
        bool
            success or failure of the operation
        """
        if self.indicator_on:
            logger.debug(f"Indicator {self.indicator_id} is already on. Skipping hardware communication")
            return True

        return self._on_mode(ArduinoBroker.ACTION_ON)

    def off(self):
        """
        Turns the indicator off.

        returns:
        bool
            success or failure of the operation
        """
        if not self.indicator_on:
            logger.debug(f"Indicator {self.indicator_id} is already off. Skipping hardware communication")
            return True

        logger.debug(f"Indicator {self.indicator_id} turning off")
        result = self.arduino_broker.indicator_action(ArduinoBroker.ACTION_OFF, self._arduino_indicator_id)
        if result:
            self.indicator_on = False
        else:
            logger.warning(f"Failed to set indicator {self.indicator_id} to OFF")

        return result

    def begin_self_test(self) -> bool:
        """
        Puts the indicator in self test mode.

        returns:
        bool
            success or failure of the operation
        """
        return self._on_mode(ArduinoBroker.ACTION_TEST)

    def warning(self) -> bool:
        """
        Sets the indicator into a pre-defined warning mode (flashing for
        instance).

        returns:
        bool
            success or failure of the operation
        """
        return self._on_mode(ArduinoBroker.ACTION_WARN)

    def _on_mode(self, action) -> bool:
        logger.debug(f"Indicator {self.indicator_id} turning on")
        result = self.arduino_broker.indicator_action(action, self._arduino_indicator_id)
        if result:
            self.indicator_on = True
        else:
            logger.warning(f"Failed to set indicator {self.indicator_id} to ON")
        return result
